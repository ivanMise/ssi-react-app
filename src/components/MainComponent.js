import React, {Component} from 'react';
import {ITEMS} from "../shared/Items";
import Catalog from "./CatalogComponent";
import ItemDetail from "./ItemdetailComponent";
import Header from "./HeaderComponent";
import Footer from "./FooterComponent";
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import Home from "./HomeComponent";
import Contact from "./ContactComponent";
import {EMPLOYEES} from "../shared/Employees";
import {COMMENTS} from "../shared/Comments";
import About from "./AboutComponent";
import {connect} from "react-redux";
import {addComment} from "../redux/ActionCreators";

//de redux al componente
const mapStateToProps = (state) => {
    return {
        items: state.items,
        employees: state.employees,
        comments: state.comments
    }
};

//del componente a redux
const mapDispatchToProps = dispatch => ({
    addComment: (itemId, rating, author, comment) => dispatch(addComment(itemId, rating, author, comment))
});

class Main extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            /*items: ITEMS,
            employees: EMPLOYEES,
            comments: COMMENTS,
            selectedItem: null*/
        };
        console.log("Main");
    }

    componentDidMount() {
        console.log("Main componentDidMount es invocado");
    }

    render() {
        console.log("Main render es invocado");
        const HomePage = () => {
            return( <Home item={this.props.items.filter(item => item.featured)[0]}
                          employee={this.props.employees.filter(employee => employee.featured)[0]}
            /> );
        };

        const ItemWithId = ({match}) => {
            return (
                <ItemDetail item={this.props.items.filter((item) => item.id === parseInt(match.params.itemId, 10))[0]}
                            comments={this.props.comments.filter((comment) => comment.itemId === parseInt(match.params.itemId, 10))}
                            addComment={this.props.addComment}
                />);
        };

        return (
            <div>
                <Header/>
                <Switch>
                    <Route path='/home' component={HomePage}/>
                    <Route path='/about' component={() => <About employees={this.props.employees}/>}/>
                    <Route exact path='/catalog' component={() => <Catalog items={this.props.items}/>}/>
                    <Route path='/catalog/:itemId' component={ItemWithId}/>
                    <Route exact path='/contactus' component={Contact} />} />
                    <Redirect to="/home"/>
                </Switch>
                <Footer/>
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
//export default Main;
