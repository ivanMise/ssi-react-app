import {ITEMS} from "../shared/Items";

export const Items = (state = ITEMS, action) => {
    switch (action.type) {
        default:
            return state;
    }
};